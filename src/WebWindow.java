import java.util.Observable;


public class WebWindow extends Model<Double> {

private Double[] window;
	
    /*window values as in positions of right and left corner respectively, 
	 * the first 2 values are the x and y coordinates of the upper left corner
	 * the last 2 the x and y coordinates of the lower right corner*/
	WebWindow(){
		this.window = new Double[] {0.0,0.0,1.0,1.0};		
	}
	
	WebWindow(Double[] coord){
		this.window = coord;
	}

	
	/**
	 * @return a
	 * <p>returns the position of the center of the window</p>
	 */
	public Double[] getWindowPosition(){
		Double x = (window[0]+ window[2])/2;
		Double y = (window[1]+ window[3])/2;
		Double a[] = new Double[]{x,y};
		return a;
	}
	
	/**
	 * @param coords
	 * <p>sets the window position given coordinates of x and y</p>
	 */
	void setWindowPosition(Double[] coords){
		if(
				(coords[1]>=0&&coords[1]<=1000) &&
				(coords[3]>=0&&coords[3]<=1000) &&
				(coords[1]<=coords[3])){
			for(int i=0;i<4;i++){
				this.window[i] = coords[i];
			}
		}
		else{
			this.restorePos(0.5);
		}
		this.updateLink();
	}
	
	/* it is assumed we want the restored window to be set with the same initial parameters
	 * but shifted upwards or downwards depending on the position of the cursor */
	
	/**
	 * @param pos
	 * <p>restores the position of the WebWindow given a Double position corresponding to the y coordinate of the
	 * center of the new window position to be had</p>
	 */
	private void restorePos(Double pos){
		if (pos<0.5){
			window  = new Double[]{0.0,0.0,1.0,1.0};
		}
		else if (pos>999.5){
			window = new Double[]{0.0,999.0,1.0,1000.0};
		}
		else{
			Double y1 = pos - 0.5;
			Double y2 = pos + 0.5;
			window  = new Double[]{0.0,y1,1.0,y2};
		}
	}
	
	@Override
	public void update(Observable transform, Object arg) {
		if (!arg.equals(this)){
			if (arg instanceof Model){
				this.restorePos(Double.valueOf(arg.toString()));
				}
			else{
				System.out.println("error: non all elements in the system are legitimate Models");
			}
		}
	}
	
	@Override
	public Double get(){
		return this.getWindowPosition()[1];
	}
	
	//sets the window position given a center
	@Override
	public void set(Double pos) {
		// TODO Auto-generated method stub
		this.setWindowPosition(new Double[]{0.0,(pos-0.5),1.0,(pos+0.5)});
	}
	
	
}
