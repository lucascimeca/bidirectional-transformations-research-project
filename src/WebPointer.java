import java.util.Observable;

public class WebPointer extends Model<Double> {

private Double cursorPos;
	
	WebPointer(){
		this.cursorPos = 0.5;
	}
	
	WebPointer(Double p){
		if(p>999.5){
			this.cursorPos = 999.5;
		}
		else if (p<0.5){
			this.cursorPos = 0.5;
		}
		else{
			this.cursorPos = p;
		}
	}
	
	
	/**
	 * @param a
	 * <p>sets the position of the cursor given a Double value</p>
	 */
	public void setPosition(Double a){
		if(a>=0.5&&a<=999.5){
			cursorPos = a;
		}
		else if (a>999.5){
			cursorPos = 999.5;
		}
		else{
			cursorPos = 0.5;
		}
		this.updateLink();               // calls the linked relational object to notify it of the change triggering the updates
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
		// checks which object has changed and acts only if the other model has changed
		if (!arg.equals(this)){
			if(arg instanceof Model){
				if(Double.valueOf(arg.toString())>=0.5&&Double.valueOf(arg.toString())<=999.5){
					cursorPos = Double.valueOf(arg.toString());
				}
				else if (Double.valueOf(arg.toString())>999.5){
					cursorPos = 999.5;
				}
				else{
					cursorPos = 0.5;
				}
			}
			else{
				System.out.println("error: non all elements in the system are legitimate Models");
			}
		}
	}

	@Override
	public Double get(){
		return cursorPos;
	}
	
	@Override
	public void set(Double pos) {
		this.setPosition(pos);
	}

}
