import static org.junit.Assert.assertTrue;

public class Main {

	public static void main(String[] args) {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);

	
		System.out.println("          POINTER POSITION --------------------WINDOW POSITION");
		System.out.println("          ----------------pointer change----------------------");
		
		for(Double i=0.0; i<50; i=i+0.2){
			pointer.setPosition(i);
			System.out.print("            <<"+pointer.get()+">>                             ");
			System.out.println("<<"+window.get()+">>");
		}
		
		for(Double i=1000.0; i>850; i=i-0.2){
			pointer.setPosition(i);
			System.out.print("            <<"+pointer.get()+">>                             ");
			System.out.println("<<"+window.get()+">>");
		}
		
		System.out.println("            ---------------window change-------------------------");
		
		for(Double i=0.0; i<20; i=i+0.2){
			window.setWindowPosition(new Double[]{0.0,0.0,1.0,i});
			System.out.print("            <<"+pointer.get()+">>                             ");
			System.out.println("<<"+window.get()+">>");
		}
		
		for(Double i=1000.0; i>880; i=i-0.2){
			window.setWindowPosition(new Double[]{0.0,i,1.0,1000.0});
			System.out.print("            <<"+pointer.get()+">>                             ");
			System.out.println("<<"+window.get()+">>");
		}
	}

}
