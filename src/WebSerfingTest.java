import static org.junit.Assert.*;

import org.junit.Test;


public class WebSerfingTest {

	
	@Test
	public void WindowPosRestoration1() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		pointer.setPosition(5.0);
		assertTrue(bx.cons());
	}
	
	@Test
	public void WindowPosRestoration2() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		pointer.setPosition(10.0);
		pointer.setPosition(2.0);
		pointer.setPosition(30.0);
		assertTrue(bx.cons());
	}
	
	
	@Test
	public void PointerPosRestoration1() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		window.setWindowPosition(new Double[]{0.0,4.0,1.0,10.0});
		assertTrue(bx.cons());
	}

	
	@Test
	public void PointerPosRestoration2() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		window.setWindowPosition(new Double[]{0.0,43.0,1.0,55.0});
		window.setWindowPosition(new Double[]{0.0,22.0,1.0,33.0});
		
		assertTrue(bx.cons());
	}
	

	// tests the property GG (GetGet)
	@Test
	public void GG_Property() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		pointer.setPosition(500.0);
		window.setWindowPosition(new Double[]{0.0,10.0,1.0,30.0});
		
		
		Double a1 = bx.getA();
		Double a2 = bx.getA();
		Double b1 = bx.getB();
		Double b2 = bx.getB();
		assertTrue(a1.equals(a2)&&b1.equals(b2));
	}
	
	// tests the property GS (GetSet)
	@Test
	public void GS_Property() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		pointer.setPosition(500.0);
		window.setWindowPosition(new Double[]{0.0,10.0,1.0,30.0});
		
		
		Double val = 500.0;
		Double a1 = bx.getA();
		Double b1 = bx.getB();
		bx.setA(val);
		bx.setB(val);
		assertTrue(!pointer.get().equals(a1)&&!window.get().equals(b1)&&pointer.get().equals(val)&&window.get().equals(val));
	}
	
	// tests the property SG (SetGet)
	@Test
	public void SG_Property() {
		
		WebPointer pointer = new WebPointer();
		WebWindow window = new WebWindow();
		Transform<Double, Double> bx = new Transform<Double, Double>();
		
		bx.addModelA(pointer);
		bx.addModelB(window);
		
		pointer.addLink(bx);
		window.addLink(bx);
		
		pointer.setPosition(500.0);
		window.setWindowPosition(new Double[]{0.0,10.0,1.0,30.0});
		

		Double val = 500.0;
		bx.setA(val);
		bx.setB(val);
		Double a1 = bx.getA();
		Double b1 = bx.getB();
		assertTrue(a1.equals(val)&&b1.equals(val));
	}
}
