import java.util.Observer;

/** model A **/
public abstract class Model<A> implements Observer {
	

	private Transform<?,?> relationalLink;
	
	/**
	 * class constructor
	 */
	Model(){
		relationalLink = null;
	}
	
	/**
	 * @param o
	 * <p>links the model to the relational object through witch consistency will be restored</p>
	 */
	void addLink(Transform<?,?> o){
		relationalLink = o;
	}
	
	
	/**
	 * method that allows to notify the observed object (relation link) of an internal change that 
	 * may or may not lead to a consistency restoration handling
	 */
	protected void updateLink(){
		if(relationalLink!=null){
			relationalLink.distributeUpdate(this);
		}
	}


	@Override
	public String toString(){
		return this.get().toString();
	}
	
	public abstract A get();
	public abstract void set(A a);
	

}
