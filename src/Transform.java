import java.util.Observable; 

public class Transform<A,B> extends Observable {
	
	private Model<A> a;
	private Model<B> b;
	
	/**
	 * class constructor
	 */
	Transform(){
		a = null;
		b = null;
	}

	// methods that does fundamentally what add observer does but keeps track of who's observing
	
	/**
	 * @param m
	 * <p> adds model A to the Bx </p>
	 */
	public void addModelA(Model<A> m){
		addObserver(m);
		a = m;
	}
	
	/**
	 * @param a
	 * <p> adds model B to the Bx </p>
	 */
	public void addModelB(Model<B> a){
		addObserver(a);
		b = a;
	}
	
	/**
	 * removes the linked models
	 */
	public void unLinkObservers(){
		a = null;
		b = null;
		deleteObserver(a);
		deleteObserver(a);
	}
	
	
	/**
	 * @param a
	 * <p>method that's called when a linked Model object has been updated, it allows update messages to be
	 * propagated to all Models should consistency have been broken</p>
	 */
	public void distributeUpdate(Object a){
		if(this.cons()!=true){
			setChanged();
			notifyObservers(a);
		}
	}

	public A getA() {
		return a.get();
	}
	
	public B getB() {
		return b.get();
	}

	public void setA(A val) {
		a.set(val);
	}

	public void setB(B val) {
		b.set(val);
	}
	
	// checks consistency between the window and the cursor position at any time
		public boolean cons() {
			return a.get().equals(b.get());
		}
}
