\documentclass[a4, 12pt]{article}
\usepackage[official]{eurosym}
\usepackage{graphicx}
\usepackage{array}
\usepackage{xcolor}
\usepackage{fancyhdr}
\usepackage{url}
\pagestyle{fancy}
\fancyhead[L]{\footnotesize Luca Scimeca}
\fancyhead[R]{\footnotesize s1344727}
\fancyheadoffset{0in}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\HRule}[1]{\rule{\linewidth}{#1}}
\addtolength{\oddsidemargin}{-.35in}
\addtolength{\evensidemargin}{-.35in}
\addtolength{\textwidth}{.65in}

\begin{document}

	\title{ \normalsize \textsc{Undergraduate Research Practical}
		\\ [2.0cm]
		\HRule{0.5pt} \\
		\LARGE \textbf{\uppercase{Bidirectional Transformations}}
		\HRule{2pt} \\ [0.5cm]
		\normalsize \today \vspace*{5\baselineskip}}
	
	\date{}
	
	\author{
		Student ID: s1344727 \\ 
		The University of Edinburgh \\
		Informatics Department }
	
	\maketitle
	\thispagestyle{empty}
	\newpage
	\tableofcontents
	\setcounter{page}{1}
	\newpage
	\begin{abstract}
		\looseness-1The research area of bidirectional transformations (bx) is explored. A recent coalgebraic approach to bx is used to implement Object Oriented examples for simple bxs previously only implemented in functional programming environments (e.g. Haskell). A Java general structure is proposed and claimed to be generic enough to implement consistency restoration strategies between \emph{any} two data sources A and B. The approach to the design strategy is discussed and extended to two examples, which are examined. At last, strengths and weaknesses of the approach are outlined.\vskip1pt
	\end{abstract}
	\hfill
	\newline
	\section{Introduction}
	\paragraph{} 
	In Software Engineering, like many other related fields, it often happens to have information in different domains that, being related, needs to be maintained consistent. \newline Imagine two databases, one of which captures some of the information contained in the other; changing an entry in one might lead to a change in the other; or imagine there are two parts of a software architecture, a UML model and a test suite, for example, in case one of the two is modified, changes might have to be propagated to the other, should this be inconsistent with the first after the update. \newline Bidirectional transformations try, with different degrees of user input independence, to restore such consistencies automatically.
	The purpose of this research is to investigate the field of bidirectional transformations and see Object Oriented implementations of some of the examples discussed in previous papers. 
	
	\section{Background}
	Bidirectional transformations (bx) can be defined as mechanisms engineered to maintain consistency between two or more sources of information \cite{BxPerspective}. In the past years there have been many bx formalisms but in a recent work by J. McKinna, F.A. Saleh and J. Gibbons, a coalgebraic description of bx was proposed \cite{CoalgebraicPaper}. In the paper, a bidirectional transformation between two data sources A and B was described as a state space X, with an initial state and two related information for either source: a function from the state space to the source, returning the value of the source; and a function to update the value of the source in the state space (possibly with side effects) (see Figure 1). \newline
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=.15]{bx}
		\caption{Draft of bidirectional transformation for two models A and B.}
		\label{bx}
	\end{figure}
	In this description then, let there be a state space \emph{x} where two data sources, say A and B, need to be consistent. To retrieve the value of the data source A, a function such as \emph{x.getA()} would return the value of A in \emph{x}. To set the value of a data source A in the same state space, instead, a function such as \emph{x.setA(A $a'$)} would change \emph{x} to reflect the changes in the source A.
	In the coalgebraic description of the bx three laws must hold, namely: GetGet, SetGet and GetSet \cite{CoalgebraicPaper}.
	\begin{itemize}
		\item GetGet: concatenated operations of \emph{x.getA()} for a source of information A and state space x. The state space should be unaltered.
		\item GetSet: operations of \emph{x.setA(A x.getA())} for a model A and state space \emph{x} such that the value set is the value $a'$ returned by \emph{x.getA()} for the same model. The \emph{x} state space should be unaltered.
		\item SetGet: operations of \emph{x.getA()} in a model A and state space \emph{x} such that the $a'$ value retrieved is the value set by \emph{x.setA(A $a'$)} for model A. The state space \emph{x} should be updated with $a'$ for A and the A value should be returned.
	\end{itemize}
	The laws correspond respectively to the GG, GS and SG laws \cite{EntangledStateMonad}. This description is powerful enough to support the notion of symmetric lenses \cite{symLenses} where the \emph{get} and \emph{put} operations can be described by \emph{get} and \emph{set} in the state space above described. \newline
	From this description of bx, given changes can be propagated from a data source A to a data source B through the state space \emph{x}, a notion of composition can be straightforwardly thought of as another data source C such that both A and C relate to B. In this situation, given a consistency relation for A-B and B-C, for a change in A there exists a value in B such that A-B are consistent, and equivalently for B-C; at this point, changes in A are propagated through the state space to C and vice-versa.
	
	
	
	\section{Object Oriented Implementation and Design}
	\paragraph{}
	
	The coalgebraic description of a bidirectional transformation is relevant to the Object Oriented view of a bx.
	The aim here, is building a OO structure capable of implementing a bx between two sources of information or \emph{models} A and B which, being somehow related, need to be consistent throughout their respective lives. The structure should be powerful enough to support any A and B subject to a consistency restoration R; that is, it should be possible to only extend such a structure in order to model A and B and maintain them consistent. \newline
	There are different possible, and likely similarly efficient, designs that could model such a case. Here we propose and analyze a particular design.
	\subsection{General Model}
	\paragraph{}
	A very useful pattern in software engineering is the \emph{Observer} pattern. A related pattern in Java is the Model-View-Controller (MVC) pattern \cite{DesignPatterns}.
	MVC is generally relevant to the consistency restoration problem and it is mainly applied by means of two special classes, Observer and Observable. In a typical Model-View-Controller scenario, a user would act on some \emph{model} through a \emph{controller}, and the \emph{views} of that structure would update automatically given changes to the underlying model. In this scenario, the model would be the Observable class while the Observer(s) would be view(s) that, given changes to the model, would update themselves accordingly (see Figure 2). \newline
	In the Java framework, an Observable lists all the Observer objects observing it; when the Observable changes, the observing objects get notified through two methods, namely \emph{hasChanged()} and \emph{notifyObservers()}. These methods call a special method in each Observer called \emph{update()} which is usually used for the views to update their state according to the changes in the observed object.
	\begin{figure}[h]
			\centering
			\setlength{\fboxsep}{5pt}
			\setlength{\fboxrule}{1pt}
			\fbox{\includegraphics[scale=.25]{MVCtypical}}
			\caption{Draft of dependencies in a Java MVC typical application.}
			\label{MVCtypical}
	\end{figure}
	\newline
	The Object Oriented implementation of a Bidirectional Transformation, in this design, is modeled with the MVC pattern through two classes: Model and Transform. The Model is a class designed to be extended by any source of information while the Transform captures the bx behavior, triggering consistency restoration messages to all models, should they need to be updated.
	In the framework, the Transform and the Model are implemented respectively as the Observable and the Observer classes in the pattern (see Figure 3);\newline
	The reason behind the choice of Observer and Observable follows from two simple facts: with bidirectional transformations we want consistency restorations to be done \emph{across} two related models, and more importantly, we want a way of dealing with updates that does not overload the relation among them; the connections need be as ``thin" as possible so to allow scalability. In this scenario, then, the Observer and Observable roles are swapped and the underlying data is not the object that is being observed but the observer of a relation though which change messages are propagated.\newline
	\begin{figure}[h]
		\centering
		\setlength{\fboxsep}{5pt}
		\setlength{\fboxrule}{1pt}
		\fbox{\includegraphics[scale=.145]{MVCbx}}
		\caption{Draft of dependencies in the Java MVC implementation of a bx relation.}
		\label{MVCbx}
	\end{figure}
	To explain the design, let us think of the dynamics in the implementation of a simple example. \newline
	A typical scenario of this implementation would see a user acting upon a model, directly or not, by for instance changing a UML aggregation arrow for a class in a particular software suite. When that happens, the internal representation of the UML model is modified and consistency might need to be restored in some other related object in the suite, e.g. the block of code relative to the class. In this design, a link between the UML object and the Transform object is then used to trigger the updates over all Model objects observing the transformation relation. The Transform object is responsible for assessing the consistency state after the event; in case the consistency between the models is lost, the \emph{update()} methods are called in all the linked observers and the \emph{state} of the object that has changed its status is passed as a parameter. Consequently, through the \emph{update()} method in each Model object, all models modify their state accordingly to the state of the changed model in the world.\newline
	The Model and Transform are designed as follows:
	\subparagraph{Model class:}
	\subparagraph{}
	Model is an abstract class designed to implement models (sources of information) in the world.
	The class extends Observer in the MVC pattern and is to be extended by more specific information source instances when they arise.\newline
	A \emph{get()} and \emph{set()} method allow to respectively get and set the state of the Model, while \emph{updateLink()} notifies the ``world" of a change, should it happen, through a pointer to a Transform class; Additionally, a method \emph{update()} is instantiated and meant to be called when a consistency restoration message is propagated to the Model objects.
	\subparagraph{Transform class:}
	\subparagraph{}
	Transform is a class that simulates the relational link between two models in the world, its purpose is mainly assessing the consistency status of the two Model objects linked to it and triggering consistency restoration messages over them should consistency be lost after a change. Transform extends the Observable class and can be observed by any Model in the system. The \emph{distributeUpdate()} method allows for notification messages to be propagated to all Model objects when they need to be notified of a change, while the \emph{cons()} method checks for consistencies to have been maintained at any given point in time.
	\newline
	\newline
	For any more specific instance of a source of information, extending the Model class imposes the implementation of methods to be used should the data source need to be linked to another data source through a Transform. However, the implemented class is not directly dependent to any other Model in the system, that is, if no Model objects were to be linked to a Transform, its ``life" would go on undisturbed, and its behavior would not be affected. This, effectively allows for any already existing class of some source of information to be linked to another source by simply extending the Model class and implementing the additional methods (its functionalities would not be broken and old API would still be valid).\newline
	\subsection{Design choice rationale}
	Another possible implementation could see the MVC model to be used as one typically would, thus by having the Transform class as Observer and the Models in the world as Observable so that, when a Model changes, the Transform is notified and can deal with it accordingly. Such an implementation, if more intuitive, could on the long run turn out to be of less use. A relational object such as the Transform, relating two data sources A and B like above, if notified of a change in A with side effects to B would need to have enough information about A and B to generate what the state of B should be after A has changed, and vice versa. In a very simple case this is an easy implementation of a bx relation, but what if A and B were more complex Model objects whose state changes could not be captured with but a few variables? Or what if we had more than just two Model extending classes relating to each other and as one changes all others would potentially need updating? In cases like these the Transform relation would need to hold so much information about the data sources that restoring consistency given a change would not be so simple; more importantly, it would be a too complex relation to be maintained on scale. Inverting the roles of Observable and Observer allows for the Transform to merely hold pointers to all interested models in the world and trigger consistency restoration massages when one happens to change. Note that here it is not the bx Transform that restores consistency, but the Model that changes its state accordingly to what is changed in the world (should the Transform assess that the change disrupted the previously consistent status). The updates are thus handled locally and no more complexity is added when more classes extend Model or more complex versions of the same are added to the bx.\newline
	One could argue that inverting the Observer-Observable roles for the Model and the Transform class, in the MVC context, would not necessarily force the Transform to hold information about A and B to restore consistency. However, if that were the case the Model class would need a method that, upon call from the Transform, handled consistency restorations, much like \emph{update()} does already. Moreover, additional code would be needed in the Transform to make sure all objects linked to it would be notified of changes, should these happen to disrupt consistency. This approach then, would effectively make the MVC pattern superfluous to the implementation, it would require additional methods in both the Transform and Model classes and would overall behave similarly if not less efficiently than the already implemented version, losing compactness and adding complexity on the way (the implementation, in fact, would be but a slightly changed copy of the proposed one).\newline
	
	\section{Positional Browsing Example}
	\subsection{Positional Browsing as Bidirectional Transformation}
	\paragraph{}
	A very simple example of bidirectional transformation is positional browsing \cite{posBrowsing}, that is, showing the content in a page relative to the position of a window and a scroll bar. Lots of different techniques are used by UI to implement such a mechanism, but a browsing window can nonetheless be seen as a bidirectional transformation, where two models, one for the web pointer and one for the web window, are linked by a relation through which consistency needs to be maintained.\newline
	\subsection{Design Architecture and Rationale}
	\paragraph{}
	In the bx general model described, the positional browsing example can be captured by two classes:  WebWindow and WebPointer. Both classes extend the more generic Model class as they need to be maintained consistent throughout their lives.
	\begin{figure}[h]
		\centering
		\includegraphics[scale=0.44]{example_draft}
		\caption{Draft of a web window and a pointer as designed in the bx implementation}
		\label{window}
	\end{figure}
	\newpage
	\subparagraph{WebWindow}
	\subparagraph{}
	WebWindow is a class that models web windows in the world. In its most simplistic form a web window is described by the position of a left upper point and low right point defining a square frame in a page. The values of the points are held by an array of doubles with four elements, the first 2 of which contain the x and y coordinates of the upper left corner in the window and the last 2, accordingly, the x and y coordinates of the lower right corner. \newline 
	The y positions of the two points in the window vary from 0 to 1000, any position above or below takes the default values of 1000 or 0 respectively. The x coordinates do not matter in the consistency relation and therefore can take any number; in a real implementation their values might be issued at priori, a range given or a rule, such as not to show anything out of the screen range, could be decided upon. Given its irrelevance to the example, their ranges will not be further discussed.
	The WebWindow class provides methods to set and get the position of the window object additionally to the methods inherited from the Model class. The \emph{get()} method returns a Double corresponding to the y position of the center of the window, which should be consistent with the position of a scroll bar, should there be one in the world, at any given time. The \emph{set(Double pos)} method sets the position of a default window with center the \emph{pos} value passed as parameter.
	The implementation of the \emph{ update()} method allows for the position restoration of the window given the y coordinate change of a pointer.
	\subparagraph{WebPointer}
	\subparagraph{}
	WebPointer is a class that describes a pointer in a scrollbar. The most basic representation of a web pointer is through a Double number relative to its y coordinate. The position values for the coordinate range from 0.5 to 999.5. 
	The Class provides methods to set and get the value of the pointer object through the \emph{set()} and \emph{get()} methods respectively. Moreover, it implements the \emph{update()} method to restore its position given the y coordinate of the center of a web window, should the pointer be linked to one, and their value be inconsistent after a positional change in the window.
	\begin{figure}[h]
	\begin{center}
		\centering
		\includegraphics[scale=0.57]{class_diagram}
		\caption{UML class diagram for Positional Browsing example}
		\label{class_diagram}
	\end{center}
	\end{figure}
	\subparagraph{}
	In a typical scenario, a user changes the position of either the WebPointer or the WebWindow object through a GUI. When a change happens, the \emph{updateLink()} method in the object that has changed state is called, and the Transform is notified of the change in the system with the \emph{distributeUpdate()} method. At this point, if consistencies are broken, all objects observing the Transform are notified of the change through the MVC's method \emph{notifyObservers()}, which makes sure the \emph{update()} in all Model objects linked to the relation are called. Each \emph{update()} takes care of restoring consistency in its own class by querying the changed Model in the world for its state and changing its own accordingly (see Figure \ref{use_case}).
	In this specific case the consistency is checked and restored every time a change happens in any of the sources. The implementation choice derives from the fact that typically, in a case such as positional browsing, the changes would have graphical outputs and the user would be immediately aware of any inconsistent positions of the two object;
	However, in other scenarios a different approach could be more efficient, more specifically, it is argued how consistency can perhaps be restored only when a view of the models or their values is requested.
	\begin{figure}[htb!]
		\begin{center}
			\centering
			
			\includegraphics[scale=0.7]{use_case}
			\caption{A Use Case for a typical scenario in the Positional Browsing OO bx implementation. The web pointer position is set and consistencies are restored through the system.}
			\label{use_case}
		\end{center}
	\end{figure}
	
	\subsection{Relationship to example repository and limitations}
	\paragraph{}
	It has been previously shown how bidirectional transformation between data sources A and B can be represented by two basic operation for each data source in a state space X, namely \emph{get} and \emph{set}. The operations correspond to \emph{getA()}, \emph{getB()}, \emph{setA()}, \emph{setB()} in the Transform class. \emph{getA()} and \emph{setA()} respectively return and set the state of the underlying data source A; similarly for B. The methods, in the OO bx framework obey the three fundamental laws as follows:
	\begin{itemize}
		\item GetGet: concatenated operations of `\emph{State bx.getA()}' for a Model A and Transform bx. The \emph{get} method should not change the state of the underlaying model when called.
		\item GetSet: concatenated operations of `\emph{State bx.getA()}' followed by `\emph{bx.setA(State a)}' for a model A, State \emph{a} and Transform bx. The operations should set the state of A to \emph{a}.
		\item SetGet: concatenated operations of `\emph{bx.setA(State a)}' followed by `\emph{State bx.getA()}' for a model A and Transform bx. The operations should return the previously set State \emph{a}.
	\end{itemize}
	In the design, the operations of \emph{set} and \emph{get} are performed by the Transform through the internal pointers to the linked Model classes. The Methods are implemented through the call of the \emph{set()} and \emph{get()} methods in the class Model which returns a generic Java type that could be matched by any type. In the design, both the model and the Transform depend on the underlying generic type which represents \emph{any} type capable of holding the state of the Model object at any given time. In positional browsing, the state of the WebWindow and the WebPointer objects can be captured by a simple \emph{Double}. \newline
	The laws for \emph{set} and \emph{get} in the Transform class are monitored by a test suit in Java. The tests, attempt to cover the behavior of the bx under the different laws separately, asserting the states of the models at different times, after subsequent operations are performed.
	
	

	\section{Extension to Composers example}
	\subsection{Composers as Bidirectional Transformation}
	\paragraph{}
	The Composer example tries to model a very common situation for two data sources maintaining consistent states. In the example, two models, A and B, represent information about the same entity in the world. The A model represents a set of musical composer objects, each with name, nationality and date. Model B is an ordered list of name-nationality pairs of composers \cite{composers}. The consistency between the two models is defined as:\newline
	 $\forall x \in B.\ \exists y \in A \mid x.nationality=y.nationality \ and \ x.name=y.name$; and similarly \newline 
	 $\forall x \in A.\ \exists y \in B \mid x.nationality=y.nationality \ and \ x.name=y.name$.
	\subsection{Design architecture and rationale}
	\paragraph{}
	In the general model context the bidirectional transformation relation between data sources A and B can be modeled through two classes, ComposerSet and ComposerList (see Figure \ref{composer}).
	\subparagraph{ComposerSet}
	\subparagraph{}
	ComposerSet is a class holding a set of Composer objects, the class implements the more generic class Model in the bx design. Each Composer holds information relative to a musical composer in the world, namely: nationality, name and date. All Composer instances are distinct elements and do not preserve ordering.\newline
	\subparagraph{ComposerList}
	\subparagraph{}
	ComposerList is a class holding information about musical composers, more specifically an array of String tuples corresponding to the nationality and the name of various composers. The class implements the more generic class Model in the bx design. The instances in the array preserve an ordering.
	\begin{figure}[h]
		\centering
		\setlength{\fboxsep}{5pt}
		\setlength{\fboxrule}{1pt}
		\fbox{\includegraphics[scale=.21]{composer}}
		\caption{Draft of Composer as a bidirectional transformation. The elements in ComposerList are stored in an array and preserve an ordering as opposed to the corresponding elements in ComposerSet. }
		\label{composer}
	\end{figure}
	
	\subparagraph{}
	Both ComposerSet and ComposerList are linked to a Transform class like previously discussed. The classes implement the \emph{update()} method to change their internal state accordingly to side-effecful changes in the world. \newline
	Two main approaches can be taken to implement the example through the design.  \newline
	In one, A and B are initially consistent and as an entry in either model is deleted or added, the Transform is informed of the change and consistency restoration messages are propagated, if need be, to restore consistency. This approach would lead to a \emph{synchronous} implementation of the relation where the \emph{State} of each model (passed through the Transform relation) needs be merely an entry pair to be read and dealt with in each class. The implemented updates in the model can be performed on single entries.\newline
	A second approach could see A and B being assumed consistent until a view of either model is requested, i.e. a \emph{get()} method in either class is queried. When a request is sent, consistency is then checked and if negative, the \emph{update()} method of the unmodified model would be called to change its internal state. This would be an \emph{asynchronous} implementation of the relation and would need the \emph{State} of the Models to be \emph{at least} a set of pairs containing information of name and nationality about all instances in either model. Note that to leave the generic structure unmodified under this implementation, updates might need to be triggered each time changes are performed to a different model; this approach, would allow the system to use the last changed object as reference for consistency restoration. (free changes can be allowed only if the \emph{State} of the models includes change history information)
	\newline
	Additionally, a decision would need to be made when restoring consistencies in A as an entry in B is added. Entries in A, in fact, hold one additional information (i.e. \emph{date}) for each composer entity. When a composer is added in B, then, its name and nationality should be propagated to a new entry in A but the date of the new entry would be unknown. A possible solution to the issue would be building the models such that, if the added entity was ever previously part of the collection, its date should be reused for the new entry; if the entry was never seen before, a default date should instead be given.
	
	\section{Discussion}
	\subsection{Composition}
	\paragraph{}
	It has previously been mentioned how the notion of composition fits within the coalgebraic description of a bx \cite{CoalgebraicPaper}. The bx OO implementation described also entails the notion of composition. The composition of three related models A,B and C, in fact, can be seen as a double implementation of the simple A-B relation. Model A can be related to Model B through a relational object \emph{transform1} extending the Transform class. Similarly,  Model B can be related to Model C through an object \emph{tranform2}. When changes in A cause the A-B relation to be inconsistent, the B Model is changed through the message propagated by \emph{transform1}; at this point, if the changed B is inconsistent with C, \emph{transform2} informs C of the change and the Model updates its state accordingly. Changes in A can then be consistently propagated through C and vice-versa (see Figure \ref{composition}).
	\begin{figure}[h]
		\centering
		\setlength{\fboxsep}{5pt}
		\setlength{\fboxrule}{1pt}
		\fbox{\includegraphics[scale=.233]{composition}}
		\caption{Diagram of the compositional view of three related models in the Object Oriented bx framework. Model A and C are indirectly related given B belongs to both \emph{transform1} and \emph{transform2} (i.e. it is listed as an Observer in both Observable objects)}
		\label{composition}
	\end{figure}
	
	\subsection{Extensions to general examples}
	\paragraph{}
	The main intuition behind the OO implemented version of a bx framework is the differentiation between the \emph{objects}, which represent the data sources to be maintained consistent, and their \emph{states}. The \emph{State} of the data sources is defined as the minimal set of information needed for the linked Model(s) to update given side-effecful changes to the source. The reason behind the differentiation of the two lies mainly in the achievement of generality when instantiating methods capable of dealing with \emph{any} two models to be maintained consistent. The consistency check in the Transform, in fact, uses the equality Java method to assess the consistency between the two linked Model objects. The equality, considers the \emph{states} of the data sources and not the objects themselves. The objects, on their hand, depend on their underlying state which is a type on its own. For any two new data sources A and B then, no matter what the information they hold is or how the consistency between the two is maintained, the Model class is extended by both to achieve the desired coupling; the Models would depend on different types of states which, given the generic underlying type, can be anything capable of holding enough information for changes to be propagated to other models. Here, equality could be used for consistency assessment in case the states are comparable types, or a new consistency could be instantiated for the Transform to be anything relating the two models. The underlying skeleton for consistency restorations then, would not need to be modified but merely extended to capture a particular bx behavior between two data sources.
	
	\subsection{Extra}
	\paragraph{}
	From category theory to algebraic descriptions, and from definitions to functional implementations, that of bidirectional transformations is a very complex research area. Many different design tools and software suits already implement some of the theories explored by the research, but often not efficiently and certainly without any notion of consistency as described in the papers. The range of possible implementations, then, is very limited and hardly generic, as industry can not afford to spend years and funds to research, and opts instead for a ``make it work" solution. \newline
	Working in this research area is invaluable and gives the opportunity to examine in depth notions only hinted on in other academic work.
	After the first big step of getting the grips with the theory, the software implementation has a whole new set of challenges to explore in the context. This implementation is a way of testing and strengthening previously only theoretical concepts. Things like \emph{generics} in Java and the actual implementation of some design patterns (i.e. \emph{observer}) were some of the most useful outcomes of the research. Others, like hypothesis testing and scientific writing were at least as important and treasured skills applicable in future work. \newline
	The OO design proposed successfully relates two models A and B whose state needs to be consistent throughout their lives. The design supports many bx behaviors discussed in various papers, and is shown to be generic enough to fit different consistency notions. The MVC pattern is a useful tool to be used for consistency restoration, however, its use limits the possible design options and drives the model architecture. Other possible designs can be abstracted away from the use of MVC, and thus become more flexible in the design decisions (e.g. how to propagate consistency restoration messages). Even maintaining the general design, the instantiation of personal methods to implement the mechanics of the Observer pattern might lead to some advantages and are certainly worth exploring.\newline
	The proposed design, achieves high coupling between all classes. This, however, might be necessary for the type of relation two models need to have against changes. Moreover, the coupling vanishes when Models are unlinked to the Transform object as all related methods are effectively unused. The sources of information, if unrelated, would then be independent of all other objects in the system. \newline
	The structure is designed to relate classes without explicitly changing their design to couple with the relation. Two already implemented classes, in fact, can extend Model, implement the missing methods, and relate to other classes without breaking their previous APIs and functionalities. This, allows for running systems to successfully relate sources of information without changing any of their behaviors. 
	
	
	\section{Conclusion}
	\paragraph{}
	Bidirectional Transformations are defined as mechanisms to restore consistency between two sources of data. A coalgebraic approach to bxs is considered, and the notions there stated used as a starting point for an Object Oriented view of a bx framework. A generic model is proposed and discussed through the implementation of two bx examples, namely: Positional Browsing and Composer. The Java implementation of the positional browsing example is discussed at length. Composer is taken into consideration as a second implementation and the different possible design decisions within the implementation are considered.\newline
	At last, the extension of the design to other generic examples is discussed. The architecture resolves communications between two data sources, consistency restoration and the notion of consistency as \emph{equality} in the Java framework.
	
	\nocite{composers}
	\nocite{BxPerspective}
	\nocite{CoalgebraicPaper}
	\nocite{DesignPatterns}
	\nocite{EntangledStateMonad}
	\nocite{NotionsOfBx}
	\nocite{posBrowsing}
	\nocite{symLenses}
	\nocite{leastChange}
	
	\medskip
	\bibliographystyle{unsrt}
	\bibliography{references}
	
	
	
\end{document}