\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {2}Background}{2}
\contentsline {section}{\numberline {3}Object Oriented Implementation and Design}{4}
\contentsline {paragraph}{}{4}
\contentsline {subsection}{\numberline {3.1}General Model}{4}
\contentsline {paragraph}{}{4}
\contentsline {subparagraph}{Model class:}{6}
\contentsline {subparagraph}{}{6}
\contentsline {subparagraph}{Transform class:}{7}
\contentsline {subparagraph}{}{7}
\contentsline {subsection}{\numberline {3.2}Design choice rationale}{7}
\contentsline {section}{\numberline {4}Positional Browsing Example}{8}
\contentsline {subsection}{\numberline {4.1}Positional Browsing as Bidirectional Transformation}{8}
\contentsline {paragraph}{}{8}
\contentsline {subsection}{\numberline {4.2}Design Architecture and Rationale}{9}
\contentsline {paragraph}{}{9}
\contentsline {subparagraph}{WebWindow}{10}
\contentsline {subparagraph}{}{10}
\contentsline {subparagraph}{WebPointer}{10}
\contentsline {subparagraph}{}{10}
\contentsline {subparagraph}{}{10}
\contentsline {subsection}{\numberline {4.3}Relationship to example repository and limitations}{13}
\contentsline {paragraph}{}{13}
\contentsline {section}{\numberline {5}Extension to Composers example}{13}
\contentsline {subsection}{\numberline {5.1}Composers as Bidirectional Transformation}{13}
\contentsline {paragraph}{}{13}
\contentsline {subsection}{\numberline {5.2}Design architecture and rationale}{14}
\contentsline {paragraph}{}{14}
\contentsline {subparagraph}{ComposerSet}{14}
\contentsline {subparagraph}{}{14}
\contentsline {subparagraph}{ComposerList}{14}
\contentsline {subparagraph}{}{14}
\contentsline {subparagraph}{}{14}
\contentsline {section}{\numberline {6}Discussion}{16}
\contentsline {subsection}{\numberline {6.1}Composition}{16}
\contentsline {paragraph}{}{16}
\contentsline {subsection}{\numberline {6.2}Extensions to general examples}{16}
\contentsline {paragraph}{}{16}
\contentsline {subsection}{\numberline {6.3}Extra}{17}
\contentsline {paragraph}{}{17}
\contentsline {section}{\numberline {7}Conclusion}{19}
\contentsline {paragraph}{}{19}
